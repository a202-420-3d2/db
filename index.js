const express = require('express');
const knexModule = require('knex');

const app = express();
const port = 3000;

app.use(express.static('public'));

const knex = knexModule({
  client: 'mysql',
  connection: {
    host: 'mysql-13600-0.cloudclusters.net',
    user: 'reader',
    password: 'Tomate123',
    database: 'school',
    port: 13600,
  },
});

app.get('/', async (req, res) => {
  const [liste] = await knex.raw('select * from students;');

  const students = [];
  for (let i = 0; i < liste.length; i += 1) {
    const student = liste[i];
    students.push(`<li>${student.first_name} ${student.last_name}</li>`);
  }

  const html = `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Title</title>
    </head>
    <body>
      <h1>Voici la liste des étudiants</h1>
      <ul>${students.join('')}</ul>
    </body>
    </html>`;

  res.send(html);
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
